import { expect } from 'chai';

import rootReducer from '../../src/reducers/index';
import * as ActionTypes from '../../src/actions/ActionTypes';

describe('Error Message reducer', () => {

  it('初期化されていること', () => {
    expect(rootReducer(undefined, {}).errorMessage)
      .to.deep.equal({});
  });

  it('errorMessage が未定義の場合 RECEIVE_ERROR イベントでエラー情報が返却されること', () => {
    let state = undefined;
    let error = new Error('test error');
    let action = {
      type: ActionTypes.RECEIVE_ERROR,
      payload: {
        message: 'error message',
        detail: error.message,
        err: error
      }
    };

    expect(rootReducer(state, action).errorMessage)
      .to.deep.equal(action.payload);
  });

  it('errorMessage が定義済みの場合 RECEIVE_ERROR イベントでエラー情報が書き換わること', () => {
    let state = {
      errorMessage: {
        type: ActionTypes.CLOSE_ERROR,
        payload: undefined
      }
    };
    let error = new Error('test error');
    let action = {
      type: ActionTypes.RECEIVE_ERROR,
      payload: {
        message: 'error message',
        detail: error.message,
        err: error
      }
    };

    expect(rootReducer(state, action).errorMessage)
      .to.deep.equal(action.payload);
  });

  it('errorMessage が未定義の場合 CLOSE_ERROR イベントで初期値が返却されること', () => {
    let state = undefined;
    let action = {
      type: ActionTypes.CLOSE_ERROR
    };

    expect(rootReducer(state, action).errorMessage)
      .to.deep.equal({});
  });

  it('errorMessage が定義済みの場合 CLOSE_ERROR イベントで初期値に戻ること', () => {
    let error = new Error('test error');
    let state = {
      errorMessage: {
      type: ActionTypes.RECEIVE_ERROR,
      payload: {
        message: 'error message',
        detail: error.message,
        err: error
      }
      }
    };
    let action = {
      type: ActionTypes.CLOSE_ERROR
    };

    expect(rootReducer(state, action).errorMessage)
      .to.deep.equal({});
  });

})
