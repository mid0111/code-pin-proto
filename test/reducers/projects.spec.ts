import { expect } from 'chai';
import * as _ from 'lodash';

import rootReducer from '../../src/reducers/index';
import * as ActionTypes from '../../src/actions/ActionTypes';

describe('Projects reducer', () => {

  it('初期化されていること', () => {
    let state = undefined;
    let action = {};
    expect(rootReducer(state, action).projects)
      .to.deep.equal({items: []});
  });

  it('projects が未定義の場合 REQUEST_PROJECTS イベントで初期値が返却されること', () => {
    let state = undefined;
    let action = {
      type: ActionTypes.REQUEST_PROJECTS
    };

    expect(rootReducer(state, action).projects)
      .to.deep.equal({items: []});
  });

  it('projects が定義済みの場合 REQUEST_PROJECTS イベントで値が変化しないこと', () => {
    let state = {
      projects: {
        items: [
          {
            "truncated": false,
            "owner": {
              "site_admin": false,
              "type": "User",
              "received_events_url": "https:\/\/api.github.com\/users\/dummyUser\/received_events",
              "events_url": "https:\/\/api.github.com\/users\/dummyUser\/events{\/privacy}",
              "repos_url": "https:\/\/api.github.com\/users\/dummyUser\/repos",
              "organizations_url": "https:\/\/api.github.com\/users\/dummyUser\/orgs",
              "subscriptions_url": "https:\/\/api.github.com\/users\/dummyUser\/subscriptions",
              "starred_url": "https:\/\/api.github.com\/users\/dummyUser\/starred{\/owner}{\/repo}",
              "gists_url": "https:\/\/api.github.com\/users\/dummyUser\/gists{\/gist_id}",
              "following_url": "https:\/\/api.github.com\/users\/dummyUser\/following{\/other_user}",
              "followers_url": "https:\/\/api.github.com\/users\/dummyUser\/followers",
              "html_url": "https:\/\/github.com\/dummyUser",
              "url": "https:\/\/api.github.com\/users\/dummyUser",
              "gravatar_id": "",
              "avatar_url": "https:\/\/avatars.githubusercontent.com\/u\/1687203?v=3",
              "id": 12345678,
              "login": "dummyUser"
            },
            "comments_url": "https:\/\/api.github.com\/gists\/a2dd93884b10e03d2d46\/comments",
            "user": null,
            "comments": 0,
            "description": "\u7279\u5b9a\u30c7\u30a3\u30ec\u30af\u30c8\u30ea\u914d\u4e0b\u306e\u30d5\u30a1\u30a4\u30eb\u7f6e\u63db",
            "updated_at": "2016-02-21T08:16:24Z",
            "created_at": "2016-02-21T08:16:23Z",
            "public": true,
            "files": [{
              "size": 53,
              "raw_url": "https:\/\/gist.githubusercontent.com\/dummyUser\/a2dd93884b10e03d2d46\/raw\/a1723f3f08cda40939c8aa192ada1ea4212da332\/rename.sh",
              "language": "Shell",
              "type": "application\/x-sh",
              "filename": "rename.sh"
            }],
            "html_url": "https:\/\/gist.github.com\/a2dd93884b10e03d2d46",
            "git_push_url": "https:\/\/gist.github.com\/a2dd93884b10e03d2d46.git",
            "git_pull_url": "https:\/\/gist.github.com\/a2dd93884b10e03d2d46.git",
            "id": "a2dd93884b10e03d2d46",
            "commits_url": "https:\/\/api.github.com\/gists\/a2dd93884b10e03d2d46\/commits",
            "forks_url": "https:\/\/api.github.com\/gists\/a2dd93884b10e03d2d46\/forks",
            "url": "https:\/\/api.github.com\/gists\/a2dd93884b10e03d2d46"
          }
        ]
      }
    };

    let action = {
      type: ActionTypes.REQUEST_PROJECTS
    };

    expect(rootReducer(state, action).projects)
      .to.deep.equal(state.projects);
  });

  it('projects が未定義の場合 RECEIVE_PROJECTS イベントでプロジェクトリストが返却されること', () => {
    let state = undefined;

    const testFile = {
      "size": 53,
      "raw_url": "https:\/\/gist.githubusercontent.com\/dummyUser\/a2dd93884b10e03d2d46\/raw\/a1723f3f08cda40939c8aa192ada1ea4212da332\/rename.sh",
      "language": "Shell",
      "type": "application\/x-sh",
      "filename": "rename.sh"
    };

    let action = {
      type: ActionTypes.RECEIVE_PROJECTS,
      payload: {
        items:[
          {
            "truncated": false,
            "owner": {
              "site_admin": false,
              "type": "User",
              "received_events_url": "https:\/\/api.github.com\/users\/dummyUser\/received_events",
              "events_url": "https:\/\/api.github.com\/users\/dummyUser\/events{\/privacy}",
              "repos_url": "https:\/\/api.github.com\/users\/dummyUser\/repos",
              "organizations_url": "https:\/\/api.github.com\/users\/dummyUser\/orgs",
              "subscriptions_url": "https:\/\/api.github.com\/users\/dummyUser\/subscriptions",
              "starred_url": "https:\/\/api.github.com\/users\/dummyUser\/starred{\/owner}{\/repo}",
              "gists_url": "https:\/\/api.github.com\/users\/dummyUser\/gists{\/gist_id}",
              "following_url": "https:\/\/api.github.com\/users\/dummyUser\/following{\/other_user}",
              "followers_url": "https:\/\/api.github.com\/users\/dummyUser\/followers",
              "html_url": "https:\/\/github.com\/dummyUser",
              "url": "https:\/\/api.github.com\/users\/dummyUser",
              "gravatar_id": "",
              "avatar_url": "https:\/\/avatars.githubusercontent.com\/u\/1687203?v=3",
              "id": 12345678,
              "login": "dummyUser"
            },
            "comments_url": "https:\/\/api.github.com\/gists\/a2dd93884b10e03d2d46\/comments",
            "user": null,
            "comments": 0,
            "description": "\u7279\u5b9a\u30c7\u30a3\u30ec\u30af\u30c8\u30ea\u914d\u4e0b\u306e\u30d5\u30a1\u30a4\u30eb\u7f6e\u63db",
            "updated_at": "2016-02-21T08:16:24Z",
            "created_at": "2016-02-21T08:16:23Z",
            "public": true,
            "files": {
              "test.sh": testFile
            },
            "html_url": "https:\/\/gist.github.com\/a2dd93884b10e03d2d46",
            "git_push_url": "https:\/\/gist.github.com\/a2dd93884b10e03d2d46.git",
            "git_pull_url": "https:\/\/gist.github.com\/a2dd93884b10e03d2d46.git",
            "id": "a2dd93884b10e03d2d46",
            "commits_url": "https:\/\/api.github.com\/gists\/a2dd93884b10e03d2d46\/commits",
            "forks_url": "https:\/\/api.github.com\/gists\/a2dd93884b10e03d2d46\/forks",
            "url": "https:\/\/api.github.com\/gists\/a2dd93884b10e03d2d46"
          }
        ]
      }
    };

    const expected = {
      items:[
        {
          "truncated": false,
          "owner": {
            "site_admin": false,
            "type": "User",
            "received_events_url": "https:\/\/api.github.com\/users\/dummyUser\/received_events",
            "events_url": "https:\/\/api.github.com\/users\/dummyUser\/events{\/privacy}",
            "repos_url": "https:\/\/api.github.com\/users\/dummyUser\/repos",
            "organizations_url": "https:\/\/api.github.com\/users\/dummyUser\/orgs",
            "subscriptions_url": "https:\/\/api.github.com\/users\/dummyUser\/subscriptions",
            "starred_url": "https:\/\/api.github.com\/users\/dummyUser\/starred{\/owner}{\/repo}",
            "gists_url": "https:\/\/api.github.com\/users\/dummyUser\/gists{\/gist_id}",
            "following_url": "https:\/\/api.github.com\/users\/dummyUser\/following{\/other_user}",
            "followers_url": "https:\/\/api.github.com\/users\/dummyUser\/followers",
            "html_url": "https:\/\/github.com\/dummyUser",
            "url": "https:\/\/api.github.com\/users\/dummyUser",
            "gravatar_id": "",
            "avatar_url": "https:\/\/avatars.githubusercontent.com\/u\/1687203?v=3",
            "id": 12345678,
            "login": "dummyUser"
          },
          "comments_url": "https:\/\/api.github.com\/gists\/a2dd93884b10e03d2d46\/comments",
          "user": null,
          "comments": 0,
          "description": "\u7279\u5b9a\u30c7\u30a3\u30ec\u30af\u30c8\u30ea\u914d\u4e0b\u306e\u30d5\u30a1\u30a4\u30eb\u7f6e\u63db",
          "updated_at": "2016-02-21T08:16:24Z",
          "created_at": "2016-02-21T08:16:23Z",
          "public": true,
          "files": [
            testFile
          ],
          "html_url": "https:\/\/gist.github.com\/a2dd93884b10e03d2d46",
          "git_push_url": "https:\/\/gist.github.com\/a2dd93884b10e03d2d46.git",
          "git_pull_url": "https:\/\/gist.github.com\/a2dd93884b10e03d2d46.git",
          "id": "a2dd93884b10e03d2d46",
          "commits_url": "https:\/\/api.github.com\/gists\/a2dd93884b10e03d2d46\/commits",
          "forks_url": "https:\/\/api.github.com\/gists\/a2dd93884b10e03d2d46\/forks",
          "url": "https:\/\/api.github.com\/gists\/a2dd93884b10e03d2d46"
        }
      ]
    };

    expect(rootReducer(state, action).projects).to.deep.equal(expected);
  });

  it('projects 定義済みの場合 RECEIVE_PROJECTS イベントでプロジェクトリストが返却されること', () => {
    let state = {
      projects: {
        items: [
          {
            "truncated": false,
            "owner": {
              "site_admin": false,
              "type": "User",
              "received_events_url": "https:\/\/api.github.com\/users\/dummyUser\/received_events",
              "events_url": "https:\/\/api.github.com\/users\/dummyUser\/events{\/privacy}",
              "repos_url": "https:\/\/api.github.com\/users\/dummyUser\/repos",
              "organizations_url": "https:\/\/api.github.com\/users\/dummyUser\/orgs",
              "subscriptions_url": "https:\/\/api.github.com\/users\/dummyUser\/subscriptions",
              "starred_url": "https:\/\/api.github.com\/users\/dummyUser\/starred{\/owner}{\/repo}",
              "gists_url": "https:\/\/api.github.com\/users\/dummyUser\/gists{\/gist_id}",
              "following_url": "https:\/\/api.github.com\/users\/dummyUser\/following{\/other_user}",
              "followers_url": "https:\/\/api.github.com\/users\/dummyUser\/followers",
              "html_url": "https:\/\/github.com\/dummyUser",
              "url": "https:\/\/api.github.com\/users\/dummyUser",
              "gravatar_id": "",
              "avatar_url": "https:\/\/avatars.githubusercontent.com\/u\/1687203?v=3",
              "id": 12345678,
              "login": "dummyUser"
            },
            "comments_url": "https:\/\/api.github.com\/gists\/a2dd93884b10e03d2d46\/comments",
            "user": null,
            "comments": 0,
            "description": "\u7279\u5b9a\u30c7\u30a3\u30ec\u30af\u30c8\u30ea\u914d\u4e0b\u306e\u30d5\u30a1\u30a4\u30eb\u7f6e\u63db",
            "updated_at": "2016-02-21T08:16:24Z",
            "created_at": "2016-02-21T08:16:23Z",
            "public": true,
            "files": [{
              "size": 53,
              "raw_url": "https:\/\/gist.githubusercontent.com\/dummyUser\/a2dd93884b10e03d2d46\/raw\/a1723f3f08cda40939c8aa192ada1ea4212da332\/rename.sh",
              "language": "Shell",
              "type": "application\/x-sh",
              "filename": "rename.sh"
            }],
            "html_url": "https:\/\/gist.github.com\/a2dd93884b10e03d2d46",
            "git_push_url": "https:\/\/gist.github.com\/a2dd93884b10e03d2d46.git",
            "git_pull_url": "https:\/\/gist.github.com\/a2dd93884b10e03d2d46.git",
            "id": "a2dd93884b10e03d2d46",
            "commits_url": "https:\/\/api.github.com\/gists\/a2dd93884b10e03d2d46\/commits",
            "forks_url": "https:\/\/api.github.com\/gists\/a2dd93884b10e03d2d46\/forks",
            "url": "https:\/\/api.github.com\/gists\/a2dd93884b10e03d2d46"
          }
        ]
      }
    };

    const testFile = {
      "size": 53,
      "raw_url": "https:\/\/gist.githubusercontent.com\/dummyUser\/a2dd93884b10e03d2d46\/raw\/a1723f3f08cda40939c8aa192ada1ea4212da332\/rename.sh",
      "language": "Shell",
      "type": "application\/x-sh",
      "filename": "rename.sh"
    };

    let action = {
      type: ActionTypes.RECEIVE_PROJECTS,
      payload: {
        items: [
          {
            "truncated": false,
            "owner": {
              "site_admin": false,
              "type": "User",
              "received_events_url": "https:\/\/api.github.com\/users\/dummyUser\/received_events",
              "events_url": "https:\/\/api.github.com\/users\/dummyUser\/events{\/privacy}",
              "repos_url": "https:\/\/api.github.com\/users\/dummyUser\/repos",
              "organizations_url": "https:\/\/api.github.com\/users\/dummyUser\/orgs",
              "subscriptions_url": "https:\/\/api.github.com\/users\/dummyUser\/subscriptions",
              "starred_url": "https:\/\/api.github.com\/users\/dummyUser\/starred{\/owner}{\/repo}",
              "gists_url": "https:\/\/api.github.com\/users\/dummyUser\/gists{\/gist_id}",
              "following_url": "https:\/\/api.github.com\/users\/dummyUser\/following{\/other_user}",
              "followers_url": "https:\/\/api.github.com\/users\/dummyUser\/followers",
              "html_url": "https:\/\/github.com\/dummyUser",
              "url": "https:\/\/api.github.com\/users\/dummyUser",
              "gravatar_id": "",
              "avatar_url": "https:\/\/avatars.githubusercontent.com\/u\/1687203?v=3",
              "id": 12345678,
              "login": "dummyUser"
            },
            "comments_url": "https:\/\/api.github.com\/gists\/a2dd93884b10e03d2d46\/comments",
            "user": null,
            "comments": 0,
            "description": "\u7279\u5b9a\u30c7\u30a3\u30ec\u30af\u30c8\u30ea\u914d\u4e0b\u306e\u30d5\u30a1\u30a4\u30eb\u7f6e\u63db",
            "updated_at": "2016-02-21T08:16:24Z",
            "created_at": "2016-02-21T08:16:23Z",
            "public": true,
            "files": {
              "test.sh": testFile
            },
            "html_url": "https:\/\/gist.github.com\/a2dd93884b10e03d2d46",
            "git_push_url": "https:\/\/gist.github.com\/a2dd93884b10e03d2d46.git",
            "git_pull_url": "https:\/\/gist.github.com\/a2dd93884b10e03d2d46.git",
            "id": "a2dd93884b10e03d2d46",
            "commits_url": "https:\/\/api.github.com\/gists\/a2dd93884b10e03d2d46\/commits",
            "forks_url": "https:\/\/api.github.com\/gists\/a2dd93884b10e03d2d46\/forks",
            "url": "https:\/\/api.github.com\/gists\/a2dd93884b10e03d2d46"
          }
        ]
      }
    };

    const expected = {
      items: [
        {
          "truncated": false,
          "owner": {
            "site_admin": false,
            "type": "User",
            "received_events_url": "https:\/\/api.github.com\/users\/dummyUser\/received_events",
            "events_url": "https:\/\/api.github.com\/users\/dummyUser\/events{\/privacy}",
            "repos_url": "https:\/\/api.github.com\/users\/dummyUser\/repos",
            "organizations_url": "https:\/\/api.github.com\/users\/dummyUser\/orgs",
            "subscriptions_url": "https:\/\/api.github.com\/users\/dummyUser\/subscriptions",
            "starred_url": "https:\/\/api.github.com\/users\/dummyUser\/starred{\/owner}{\/repo}",
            "gists_url": "https:\/\/api.github.com\/users\/dummyUser\/gists{\/gist_id}",
            "following_url": "https:\/\/api.github.com\/users\/dummyUser\/following{\/other_user}",
            "followers_url": "https:\/\/api.github.com\/users\/dummyUser\/followers",
            "html_url": "https:\/\/github.com\/dummyUser",
            "url": "https:\/\/api.github.com\/users\/dummyUser",
            "gravatar_id": "",
            "avatar_url": "https:\/\/avatars.githubusercontent.com\/u\/1687203?v=3",
            "id": 12345678,
            "login": "dummyUser"
          },
          "comments_url": "https:\/\/api.github.com\/gists\/a2dd93884b10e03d2d46\/comments",
          "user": null,
          "comments": 0,
          "description": "\u7279\u5b9a\u30c7\u30a3\u30ec\u30af\u30c8\u30ea\u914d\u4e0b\u306e\u30d5\u30a1\u30a4\u30eb\u7f6e\u63db",
          "updated_at": "2016-02-21T08:16:24Z",
          "created_at": "2016-02-21T08:16:23Z",
          "public": true,
          "files": [
            testFile
          ],
          "html_url": "https:\/\/gist.github.com\/a2dd93884b10e03d2d46",
          "git_push_url": "https:\/\/gist.github.com\/a2dd93884b10e03d2d46.git",
          "git_pull_url": "https:\/\/gist.github.com\/a2dd93884b10e03d2d46.git",
          "id": "a2dd93884b10e03d2d46",
          "commits_url": "https:\/\/api.github.com\/gists\/a2dd93884b10e03d2d46\/commits",
          "forks_url": "https:\/\/api.github.com\/gists\/a2dd93884b10e03d2d46\/forks",
          "url": "https:\/\/api.github.com\/gists\/a2dd93884b10e03d2d46"
        }
      ]
    };
    expect(rootReducer(state, action).projects).to.deep.equal(expected);
  });

})
