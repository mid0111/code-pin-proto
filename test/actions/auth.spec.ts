const configureMockStore = require('redux-mock-store');
import thunk from 'redux-thunk';
import { Action } from 'redux-actions'
import * as nock from 'nock';
import { expect } from 'chai';
import * as sinon from 'sinon';
import * as url from 'url';

import * as ActionTypes from '../../src/actions/ActionTypes';
import * as actions from '../../src/actions/auth';
import { github } from '../../src/utils/consts';
import * as Storage from '../../src/utils/storage';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

const token = '125678uhtrewsxfggt';

describe('auth actions', () => {

  let sandbox;

  beforeEach(function () {
    sandbox = sinon.sandbox.create();
  });

  afterEach(() => {
    nock.cleanAll();
    sandbox.restore();
  });

  it('checkAuthorized で loginSuccess イベントが dispatch されること', () => {
    const token = 'e72e16c7e42f292c6912e7710c838347ae178b4a';
    sandbox.stub(Storage, 'getItem').returns(token);
    sandbox.stub(Storage, 'setItem');

    const mockProfileResponse = {
      login: "qwertyui"
    };

    nock(github.baseUrl)
      .get('/user')
      .reply(200, mockProfileResponse);

    const store = mockStore({});

    const code = 'aaaa';
    return store.dispatch(actions.checkAuthorized())
      .then(() => {
        let actualActions = store.getActions();
        expect(actualActions).to.have.length(3);

        let index = 0;
        expect(actualActions[index]['type']).to.equal(ActionTypes.LOGIN_SUCCESS);
        expect(actualActions[index]['payload']).to.deep.equal({
          token: token
        });

        index ++;
        expect(actualActions[index]['type']).to.equal(ActionTypes.REQUEST_PROFILE);
        expect(actualActions[index]['payload']).to.be.undefined;

        index ++;
        expect(actualActions[index]['type']).to.equal(ActionTypes.RECEIVE_PROFILE);
        expect(actualActions[index]['payload']).to.deep.equal(mockProfileResponse);

      });
  });

  it('checkAuthorized でトークンが不正な場合 LOGOUT イベントが dispatch されること', () => {
    const token = 'e72e16c7e42f292c6912e7710c838347ae178b4a';
    sandbox.stub(Storage, 'getItem').returns(token);
    sandbox.stub(Storage, 'setItem');

    const mockProfileResponse = new Error('errorrrrrrr');

    nock(github.baseUrl)
      .get('/user')
      .reply(400, mockProfileResponse);

    const store = mockStore({});

    const code = 'aaaa';
    return store.dispatch(actions.checkAuthorized())
      .then(() => {
        let actualActions = store.getActions();
        expect(actualActions).to.have.length(3);

        let index = 0;
        expect(actualActions[index]['type']).to.equal(ActionTypes.LOGIN_SUCCESS);
        expect(actualActions[index]['payload']).to.deep.equal({
          token: token
        });

        index ++;
        expect(actualActions[index]['type']).to.equal(ActionTypes.REQUEST_PROFILE);
        expect(actualActions[index]['payload']).to.be.undefined;

        index ++;
        expect(actualActions[index]['type']).to.equal(ActionTypes.LOGOUT);
        expect(actualActions[index]['payload']).to.be.undefined;

      });
  });

  it('loginSuccess イベントが dispatch されること', () => {
    sandbox.stub(Storage, 'getItem').returns(undefined);
    sandbox.stub(Storage, 'setItem');

    const token = 'e72e16c7e42f292c6912e7710c838347ae178b4a';
    const mockAuthResponse = {
      access_token: "2d5f3464d3ec34ae6d057a440d074990408dbe39",
      scope: "gist",
      token_type: "bearer"
    };
    const mockProfileResponse = {
      login: "qwertyui"
    };

    const requestUrl = url.parse(github.oauthUrl);
    nock(requestUrl.protocol + '//' +  requestUrl.host)
      .post(requestUrl.path)
      .reply(200, mockAuthResponse);
    nock(github.baseUrl)
      .get('/user')
      .reply(200, mockProfileResponse);

    const store = mockStore({});

    const code = 'aaaa';
    return store.dispatch(actions.loginUser(code))
      .then(() => {
        let actualActions = store.getActions();
        expect(actualActions).to.have.length(4);

        let index = 0;
        expect(actualActions[index]['type']).to.equal(ActionTypes.LOGIN_REQUEST);
        expect(actualActions[index]['payload']).to.be.undefined;

        index ++;
        expect(actualActions[index]['type']).to.equal(ActionTypes.LOGIN_SUCCESS);
        expect(actualActions[index]['payload']).to.deep.equal({
          token: mockAuthResponse.access_token
        });

        index ++;
        expect(actualActions[index]['type']).to.equal(ActionTypes.REQUEST_PROFILE);
        expect(actualActions[index]['payload']).to.be.undefined;

        index ++;
        expect(actualActions[index]['type']).to.equal(ActionTypes.RECEIVE_PROFILE);
        expect(actualActions[index]['payload']).to.deep.equal(mockProfileResponse);

      });
  });

  it('ログインに失敗した場合エラーが通知されること', () => {
    sandbox.stub(Storage, 'getItem').returns(undefined);
    sandbox.stub(Storage, 'setItem');

    const token = 'e72e16c7e42f292c6912e7710c838347ae178b4a';
    let mockResponse = 'Bad Request';

    const requestUrl = url.parse(github.oauthUrl);
    nock(requestUrl.protocol + '//' +  requestUrl.host)
      .post(requestUrl.path)
      .reply(400, mockResponse);

    const store = mockStore({});

    const code = 'aaaa';
    return store.dispatch(actions.loginUser(code))
      .then(() => {
        let actualActions = store.getActions();
        expect(actualActions).to.have.length(2);

        expect(actualActions[0]['type']).to.equal(ActionTypes.LOGIN_REQUEST);
        expect(actualActions[0]['payload']).to.be.undefined;

        expect(actualActions[1]['type']).to.equal(ActionTypes.LOGIN_FAILURE);
        expect(actualActions[1]['payload'].message).to.be.string;
        expect(actualActions[1]['payload'].detail).to.equal(mockResponse);
      });
  });
});

