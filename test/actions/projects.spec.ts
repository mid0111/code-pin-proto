const configureMockStore = require('redux-mock-store');
import thunk from 'redux-thunk';
import { Action } from 'redux-actions'
import * as nock from 'nock';
import { expect } from 'chai';

import * as ActionTypes from '../../src/actions/ActionTypes';
import * as actions from '../../src/actions/projects';
import { github } from '../../src/utils/consts';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

const baseUrl = github.baseUrl;

describe('projects actions', () => {

  afterEach(() => {
    nock.cleanAll();
  });

  it('should create action to request projects and to receive project', () => {
    const mockResponse = [
      {
        "truncated": false,
        "owner": {
          "site_admin": false,
          "type": "User",
          "received_events_url": "https:\/\/api.github.com\/users\/dummyUser\/received_events",
          "events_url": "https:\/\/api.github.com\/users\/dummyUser\/events{\/privacy}",
          "repos_url": "https:\/\/api.github.com\/users\/dummyUser\/repos",
          "organizations_url": "https:\/\/api.github.com\/users\/dummyUser\/orgs",
          "subscriptions_url": "https:\/\/api.github.com\/users\/dummyUser\/subscriptions",
          "starred_url": "https:\/\/api.github.com\/users\/dummyUser\/starred{\/owner}{\/repo}",
          "gists_url": "https:\/\/api.github.com\/users\/dummyUser\/gists{\/gist_id}",
          "following_url": "https:\/\/api.github.com\/users\/dummyUser\/following{\/other_user}",
          "followers_url": "https:\/\/api.github.com\/users\/dummyUser\/followers",
          "html_url": "https:\/\/github.com\/dummyUser",
          "url": "https:\/\/api.github.com\/users\/dummyUser",
          "gravatar_id": "",
          "avatar_url": "https:\/\/avatars.githubusercontent.com\/u\/1687203?v=3",
          "id": 12345678,
          "login": "dummyUser"
        },
        "comments_url": "https:\/\/api.github.com\/gists\/a2dd93884b10e03d2d46\/comments",
        "user": null,
        "comments": 0,
        "description": "\u7279\u5b9a\u30c7\u30a3\u30ec\u30af\u30c8\u30ea\u914d\u4e0b\u306e\u30d5\u30a1\u30a4\u30eb\u7f6e\u63db",
        "updated_at": "2016-02-21T08:16:24Z",
        "created_at": "2016-02-21T08:16:23Z",
        "public": true,
        "files": {
          "rename.sh": {
            "size": 53,
            "raw_url": "https:\/\/gist.githubusercontent.com\/dummyUser\/a2dd93884b10e03d2d46\/raw\/a1723f3f08cda40939c8aa192ada1ea4212da332\/rename.sh",
            "language": "Shell",
            "type": "application\/x-sh",
            "filename": "rename.sh"
          }
        },
        "html_url": "https:\/\/gist.github.com\/a2dd93884b10e03d2d46",
        "git_push_url": "https:\/\/gist.github.com\/a2dd93884b10e03d2d46.git",
        "git_pull_url": "https:\/\/gist.github.com\/a2dd93884b10e03d2d46.git",
        "id": "a2dd93884b10e03d2d46",
        "commits_url": "https:\/\/api.github.com\/gists\/a2dd93884b10e03d2d46\/commits",
        "forks_url": "https:\/\/api.github.com\/gists\/a2dd93884b10e03d2d46\/forks",
        "url": "https:\/\/api.github.com\/gists\/a2dd93884b10e03d2d46"
      }
    ];

    const mockHeaders = {
      Link: '<https://api.github.com/resource?page=2>; rel="next", <https://api.github.com/resource?page=5>; rel="last"'
    };

    nock(baseUrl)
      .get('/gists')
      .reply(200, mockResponse, mockHeaders)
      .get('/resource')
      .query(true)
      .reply(200, mockResponse, {Link: ''});

    const store = mockStore({projects: []});
    const token = '1234567890qwertyuio';
    return store.dispatch(actions.fetchProjects(token))
      .then(() => {
        let actualActions = store.getActions();
        expect(actualActions).to.have.length(4);

        expect(actualActions[0]['type']).to.equal(ActionTypes.REQUEST_PROJECTS);
        expect(actualActions[0]['payload']).to.be.undefined;

        expect(actualActions[1]['type']).to.equal(ActionTypes.RECEIVE_PROJECTS);
        expect(actualActions[1]['payload'].items).to.deep.equal(mockResponse);
        expect(actualActions[1]['payload'].next).to.be.string;

        expect(actualActions[2]['type']).to.equal(ActionTypes.REQUEST_PROJECTS);
        expect(actualActions[2]['payload']).to.be.undefined;

        expect(actualActions[3]['type']).to.equal(ActionTypes.RECEIVE_PROJECTS);
        expect(actualActions[3]['payload'].items).to.deep.equal(mockResponse.concat(mockResponse));
        expect(actualActions[3]['payload'].next).to.be.undefined;
      });
  });

  it('should report error when request failed.', () => {
    let mockResponse = 'Bad Request';

    nock(baseUrl)
      .get('/gists')
      .reply(400, mockResponse);

    const store = mockStore({projects: []});

    const token = '1234567890qwertyuio';
    return store.dispatch(actions.fetchProjects(token))
      .then(() => {
        let actualActions = store.getActions();
        expect(actualActions).to.have.length(2);

        expect(actualActions[0]['type']).to.equal(ActionTypes.REQUEST_PROJECTS);
        expect(actualActions[0]['payload']).to.be.undefined;

        expect(actualActions[1]['type']).to.equal(ActionTypes.RECEIVE_ERROR);
        expect(actualActions[1]['payload'].message).to.be.string;
        expect(actualActions[1]['payload'].detail).to.equal(mockResponse);
      });
  });
});

