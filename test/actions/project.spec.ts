const configureMockStore = require('redux-mock-store');
import thunk from 'redux-thunk';
import { Action } from 'redux-actions'
import * as nock from 'nock';
import { expect } from 'chai';

import * as ActionTypes from '../../src/actions/ActionTypes';
import * as actions from '../../src/actions/project';
import { github } from '../../src/utils/consts';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

const baseUrl = github.baseUrl;

describe('project actions', () => {

  afterEach(() => {
    nock.cleanAll();
  });

  it('should create action to request projects and to receive project', () => {
    const id = 'id1234567890';
    const mockResponse = {
      "url": "https://api.github.com/gists/aa5a315d61ae9438b18d",
      "forks_url": "https://api.github.com/gists/aa5a315d61ae9438b18d/forks",
      "commits_url": "https://api.github.com/gists/aa5a315d61ae9438b18d/commits",
      "id": id,
      "description": "description of gist",
      "public": true,
      "owner": {
        "login": "octocat",
        "id": 1,
        "avatar_url": "https://github.com/images/error/octocat_happy.gif",
        "gravatar_id": "",
        "url": "https://api.github.com/users/octocat",
        "html_url": "https://github.com/octocat",
        "followers_url": "https://api.github.com/users/octocat/followers",
        "following_url": "https://api.github.com/users/octocat/following{/other_user}",
        "gists_url": "https://api.github.com/users/octocat/gists{/gist_id}",
        "starred_url": "https://api.github.com/users/octocat/starred{/owner}{/repo}",
        "subscriptions_url": "https://api.github.com/users/octocat/subscriptions",
        "organizations_url": "https://api.github.com/users/octocat/orgs",
        "repos_url": "https://api.github.com/users/octocat/repos",
        "events_url": "https://api.github.com/users/octocat/events{/privacy}",
        "received_events_url": "https://api.github.com/users/octocat/received_events",
        "type": "User",
        "site_admin": false
      },
      "user": null,
      "files": {
        "ring.erl": {
          "size": 932,
          "raw_url": "https://gist.githubusercontent.com/raw/365370/8c4d2d43d178df44f4c03a7f2ac0ff512853564e/ring.erl",
          "type": "text/plain",
          "language": "Erlang",
          "truncated": false,
          "content": "contents of gist"
        }
      },
      "truncated": false,
      "comments": 0,
      "comments_url": "https://api.github.com/gists/aa5a315d61ae9438b18d/comments/",
      "html_url": "https://gist.github.com/aa5a315d61ae9438b18d",
      "git_pull_url": "https://gist.github.com/aa5a315d61ae9438b18d.git",
      "git_push_url": "https://gist.github.com/aa5a315d61ae9438b18d.git",
      "created_at": "2010-04-14T02:15:15Z",
      "updated_at": "2011-06-20T11:34:15Z",
      "forks": [
        {
          "user": {
            "login": "octocat",
            "id": 1,
            "avatar_url": "https://github.com/images/error/octocat_happy.gif",
            "gravatar_id": "",
            "url": "https://api.github.com/users/octocat",
            "html_url": "https://github.com/octocat",
            "followers_url": "https://api.github.com/users/octocat/followers",
            "following_url": "https://api.github.com/users/octocat/following{/other_user}",
            "gists_url": "https://api.github.com/users/octocat/gists{/gist_id}",
            "starred_url": "https://api.github.com/users/octocat/starred{/owner}{/repo}",
            "subscriptions_url": "https://api.github.com/users/octocat/subscriptions",
            "organizations_url": "https://api.github.com/users/octocat/orgs",
            "repos_url": "https://api.github.com/users/octocat/repos",
            "events_url": "https://api.github.com/users/octocat/events{/privacy}",
            "received_events_url": "https://api.github.com/users/octocat/received_events",
            "type": "User",
            "site_admin": false
          },
          "url": "https://api.github.com/gists/dee9c42e4998ce2ea439",
          "id": "dee9c42e4998ce2ea439",
          "created_at": "2011-04-14T16:00:49Z",
          "updated_at": "2011-04-14T16:00:49Z"
        }
      ],
      "history": [
        {
          "url": "https://api.github.com/gists/aa5a315d61ae9438b18d/57a7f021a713b1c5a6a199b54cc514735d2d462f",
          "version": "57a7f021a713b1c5a6a199b54cc514735d2d462f",
          "user": {
            "login": "octocat",
            "id": 1,
            "avatar_url": "https://github.com/images/error/octocat_happy.gif",
            "gravatar_id": "",
            "url": "https://api.github.com/users/octocat",
            "html_url": "https://github.com/octocat",
            "followers_url": "https://api.github.com/users/octocat/followers",
            "following_url": "https://api.github.com/users/octocat/following{/other_user}",
            "gists_url": "https://api.github.com/users/octocat/gists{/gist_id}",
            "starred_url": "https://api.github.com/users/octocat/starred{/owner}{/repo}",
            "subscriptions_url": "https://api.github.com/users/octocat/subscriptions",
            "organizations_url": "https://api.github.com/users/octocat/orgs",
            "repos_url": "https://api.github.com/users/octocat/repos",
            "events_url": "https://api.github.com/users/octocat/events{/privacy}",
            "received_events_url": "https://api.github.com/users/octocat/received_events",
            "type": "User",
            "site_admin": false
          },
          "change_status": {
            "deletions": 0,
            "additions": 180,
            "total": 180
          },
          "committed_at": "2010-04-14T02:15:15Z"
        }
      ]
    };

    nock(baseUrl)
      .get('/gists/' + id)
      .reply(200, mockResponse);

    const store = mockStore({project: {}});
    const token = '1234567890qwertyuio';
    return store.dispatch(actions.fetchProjectDetail(token, id))
      .then(() => {
        let actualActions = store.getActions();
        expect(actualActions).to.have.length(2);

        expect(actualActions[0]['type']).to.equal(ActionTypes.REQUEST_PROJECT_DETAIL);
        expect(actualActions[0]['payload']).to.be.undefined;

        expect(actualActions[1]['type']).to.equal(ActionTypes.RECEIVE_PROJECT_DETAIL);
        expect(actualActions[1]['payload'].item).to.deep.equal(mockResponse);
      });
  });

  it('should report error when request failed.', () => {
    let mockResponse = 'Bad Request';
    const id = 'id1234567890';

    nock(baseUrl)
      .get('/gists/' + id)
      .reply(400, mockResponse);

    const store = mockStore({project: {}});

    const token = '1234567890qwertyuio';
    return store.dispatch(actions.fetchProjectDetail(token, id))
      .then(() => {
        let actualActions = store.getActions();
        expect(actualActions).to.have.length(2);

        expect(actualActions[0]['type']).to.equal(ActionTypes.REQUEST_PROJECT_DETAIL);
        expect(actualActions[0]['payload']).to.be.undefined;

        expect(actualActions[1]['type']).to.equal(ActionTypes.RECEIVE_ERROR);
        expect(actualActions[1]['payload'].message).to.be.string;
        expect(actualActions[1]['payload'].detail).to.equal(mockResponse);
      });
  });
});

