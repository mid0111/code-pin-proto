//
// 'Error: Cannot find module 'electron'' エラーが発生してしまうためやむなくコメントアウト
//

// import * as React from 'react';
// import { Dispatch } from 'redux';
// import { remote } from 'electron';

// import { shallow, mount } from 'enzyme';
// import * as sinon from 'sinon';
// import { expect } from 'chai';

// import { App, Auth } from './App';
// import { ErrorMessage } from '../components/AlertMessage';

// function setup() {

//   const mockChildren = React.createElement('testElement');
//   const mockDispatch = sinon.spy() as Dispatch;
//   const mockAuth = sinon.spy() as Auth;

//   return {
//     mockChildren,
//     mockDispatch,
//     mockAuth
//   };
// }

// describe('App component', () => {
//   it('Header が表示されること', () => {
//     const mockErrorMessage = sinon.spy() as ErrorMessage;
//     const { mockChildren, mockDispatch, mockAuth } = setup();

//     const wrapper = shallow(
//       <App children={mockChildren}
//         errorMessage={mockErrorMessage}
//         auth={mockAuth}
//         dispatch={mockDispatch}/>
//     );

//     let titleArea = wrapper.find('Header');
//     expect(titleArea.prop('title') ).to.equal('Code Pin');
//     expect(titleArea.prop('loginName')).to.be.string;
//   });

//   it('children が表示されること', () => {
//     const mockErrorMessage = sinon.spy() as ErrorMessage;
//     const { mockChildren, mockDispatch, mockAuth } = setup();

//     const wrapper = shallow(
//       <App children={mockChildren}
//         errorMessage={mockErrorMessage}
//         auth={mockAuth}
//         dispatch={mockDispatch}/>
//     );

//     let children = wrapper.find(mockChildren.type);
//     expect(children).to.have.length(1);
//   });

//   it('AlertMessage が表示されること', () => {
//     const error = new Error('test error');
//     const mockErrorMessage : ErrorMessage = {
//       message: 'test error message',
//       detail: error.message,
//       error: error
//     };
//     const { mockChildren, mockDispatch, mockAuth } = setup();

//     const wrapper = shallow(
//       <App children={mockChildren}
//         errorMessage={mockErrorMessage}
//         auth={mockAuth}
//         dispatch={mockDispatch}/>
//     );

//     let alertArea = wrapper.find('AlertMessage');
//     expect(alertArea.prop('errorMessage')).to.deep.equal(mockErrorMessage);
//   });

// });
