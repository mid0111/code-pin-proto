import * as React from 'react';
import { Dispatch } from 'redux';

import { shallow, mount } from 'enzyme';
import * as sinon from 'sinon';
import { expect } from 'chai';

import { Dashboard } from '../../src/containers/Dashboard';
import { UserInfo } from '../../src/containers/App';

describe('Dashboard component', () => {

  it('ProjectList が描画されること', () => {
    const mockProjects = {
      items: []
    };
    const mockProject: any = {};
    const mockAuth = {
      token: undefined,
      user: {},
      fetching: true
    } as UserInfo;
    const mockDispatch = sinon.spy() as Dispatch;

    const wrapper = shallow(
      <Dashboard projects={mockProjects}
        selectedProject={mockProject}
        userInfo={mockAuth}
        dispatch={mockDispatch} />
    );

    let projectContainer = wrapper.find('ProjectList');
    expect(projectContainer).to.have.length(1);
    expect(projectContainer.prop('projects')).to.deep.equal(mockProjects);
  });

});
