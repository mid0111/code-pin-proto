import * as React from 'react';
import { shallow } from 'enzyme';

import { expect } from 'chai';
import * as sinon from 'sinon';

import { AlertMessage, ErrorMessage } from '../../src/components/AlertMessage';

describe('AlertMessage component', () => {

  it('メッセージが空の場合アラートが表示されないこと', () => {
    const mockErrorMessage : ErrorMessage = {};
    const mockCloseMessage = sinon.spy() as Function;

    const wrapper = shallow(
      <AlertMessage errorMessage={mockErrorMessage}
        onCloseMessage={mockCloseMessage}/>
    );

    expect(wrapper.prop('show')).to.be.false;
  });

  it('メッセージが存在する場合アラートが表示されること', () => {
    const error = new Error('test error');
    const mockErrorMessage : ErrorMessage = {
      message: 'test error message',
      error: error
    };
    const mockCloseMessage = sinon.spy() as Function;

    const wrapper = shallow(
      <AlertMessage errorMessage={mockErrorMessage}
        onCloseMessage={mockCloseMessage}/>
    );

    expect(wrapper.prop('show')).to.be.true;

    let alertTitle = wrapper.find('.alert-title');
    expect(alertTitle.props().children).to.equal(mockErrorMessage.message);

    let alertDetail = wrapper.find('.alert-detail');
    expect(alertDetail.props().children).to.equal('');
  });

  it('メッセージ及び詳細メッセージが存在する場合アラートが表示されること', () => {
    const error = new Error('test error');
    const mockErrorMessage : ErrorMessage = {
      message: 'test error message',
      detail: error.message,
      error: error
    };
    const mockCloseMessage = sinon.spy() as Function;

    const wrapper = shallow(
      <AlertMessage errorMessage={mockErrorMessage}
        onCloseMessage={mockCloseMessage}/>
    );

    expect(wrapper.prop('show')).to.be.true;

    let alertTitle = wrapper.find('.alert-title');
    expect(alertTitle.props().children).to.equal(mockErrorMessage.message);

    let alertDetail = wrapper.find('.alert-detail');
    expect(alertDetail.props().children).to.equal(mockErrorMessage.detail);
  });

});
