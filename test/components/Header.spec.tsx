import * as React from 'react';
import { shallow } from 'enzyme';

import { expect } from 'chai';

import { Header, User } from '../../src/components/Header';

function setup() {
  const props = {
    title: 'test title',
    user: {
      login: 'test login name'
    }
  };

  const wrapper = shallow(<Header title={props.title} user={props.user} />);

  return {
    props,
    wrapper
  };
}

describe('Header component', () => {
  it('タイトルが表示されること',() => {
    const { props, wrapper } = setup();

    let titleArea = wrapper.find('.title');
    let titleHref = titleArea.find('a');
    expect(titleHref.props().children).to.equal(props.title);
  });

  it('ユーザー名が表示されること',() => {
    const { props, wrapper } = setup();

    let titleArea = wrapper.find('#user-menu');
    expect(titleArea.props().title).to.equal(props.user.login);

    let menuItems = titleArea.find('MenuItem');
    expect(menuItems.get(0).props.children).to.equal('Settings');
    expect(menuItems.get(1).props.children).to.equal('Log out');
  });

});
