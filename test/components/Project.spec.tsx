import * as React from 'react';
import { Dispatch } from 'redux';

import { shallow, mount } from 'enzyme';
import * as sinon from 'sinon';
import { expect } from 'chai';

import { Project } from '../../src/components/Project';
import { ProjectInfo } from '../../src/components/ProjectDetail';

describe('Project component', () => {

  it('Project が表示できること', () => {
    const { mockProject, onClick } = setup();
    const isActive = false;

    const wrapper = shallow(
      <Project project={mockProject} onClick={onClick} isActive={isActive} />
    );

    wrapper.find('.project-item').simulate('click');
    expect(onClick.calledOnce).to.be.true;

    let name = wrapper.find('.project-name');
    expect(name.props().children).to.equal(mockProject.files[Object.keys(mockProject.files)[0]].filename);

    let description = wrapper.find('.project-description');
    expect(description.props().children).to.equal(mockProject.description);
  });

  it('Project がアクティブの場合クラス属性がセットされていること', () => {
    const { mockProject, onClick } = setup();
    const isActive = true;

    const wrapper = shallow(
      <Project project={mockProject} onClick={onClick} isActive={isActive} />
    );

    const item = wrapper.find('.project-item').simulate('click');
    expect(item.props().className).to.contains('is-active');
    expect(onClick.calledOnce).to.be.true;

    let name = wrapper.find('.project-name');
    expect(name.props().children).to.equal(mockProject.files[Object.keys(mockProject.files)[0]].filename);

    let description = wrapper.find('.project-description');
    expect(description.props().children).to.equal(mockProject.description);
  });


  function setup() {
    const mockProject: ProjectInfo = {
      "truncated": false,
      "owner": {
        "site_admin": false,
        "type": "User",
        "received_events_url": "https:\/\/api.github.com\/users\/dummyUser\/received_events",
        "events_url": "https:\/\/api.github.com\/users\/dummyUser\/events{\/privacy}",
        "repos_url": "https:\/\/api.github.com\/users\/dummyUser\/repos",
        "organizations_url": "https:\/\/api.github.com\/users\/dummyUser\/orgs",
        "subscriptions_url": "https:\/\/api.github.com\/users\/dummyUser\/subscriptions",
        "starred_url": "https:\/\/api.github.com\/users\/dummyUser\/starred{\/owner}{\/repo}",
        "gists_url": "https:\/\/api.github.com\/users\/dummyUser\/gists{\/gist_id}",
        "following_url": "https:\/\/api.github.com\/users\/dummyUser\/following{\/other_user}",
        "followers_url": "https:\/\/api.github.com\/users\/dummyUser\/followers",
        "html_url": "https:\/\/github.com\/dummyUser",
        "url": "https:\/\/api.github.com\/users\/dummyUser",
        "gravatar_id": "",
        "avatar_url": "https:\/\/avatars.githubusercontent.com\/u\/1687203?v=3",
        "id": 12345678,
        "login": "dummyUser"
      },
      "comments_url": "https:\/\/api.github.com\/gists\/a2dd93884b10e03d2d46\/comments",
      "user": null,
      "comments": 0,
      "description": "\u7279\u5b9a\u30c7\u30a3\u30ec\u30af\u30c8\u30ea\u914d\u4e0b\u306e\u30d5\u30a1\u30a4\u30eb\u7f6e\u63db",
      "updated_at": "2016-02-21T08:16:24Z",
      "created_at": "2016-02-21T08:16:23Z",
      "public": true,
      "files": [{
        "size": 53,
        "raw_url": "https:\/\/gist.githubusercontent.com\/dummyUser\/a2dd93884b10e03d2d46\/raw\/a1723f3f08cda40939c8aa192ada1ea4212da332\/rename.sh",
        "language": "Shell",
        "type": "application\/x-sh",
        "filename": "rename.sh"
      }],
      "html_url": "https:\/\/gist.github.com\/a2dd93884b10e03d2d46",
      "git_push_url": "https:\/\/gist.github.com\/a2dd93884b10e03d2d46.git",
      "git_pull_url": "https:\/\/gist.github.com\/a2dd93884b10e03d2d46.git",
      "id": "a2dd93884b10e03d2d46",
      "commits_url": "https:\/\/api.github.com\/gists\/a2dd93884b10e03d2d46\/commits",
      "forks_url": "https:\/\/api.github.com\/gists\/a2dd93884b10e03d2d46\/forks",
      "url": "https:\/\/api.github.com\/gists\/a2dd93884b10e03d2d46"
    };
    const onClick = sinon.spy();

    return {
      mockProject,
      onClick
    };
  }
});
