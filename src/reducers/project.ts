import { handleActions, Action } from 'redux-actions';
import * as _ from 'lodash';

import * as ActionTypes from '../actions/ActionTypes';
import { ProjectDetailProps } from '../components/ProjectDetail';

const project = handleActions<ProjectDetailProps>({
  [ActionTypes.REQUEST_PROJECT_DETAIL]: (state, action) => {
    return state;
  },

  [ActionTypes.RECEIVE_PROJECT_DETAIL]: (state, action) => {
    const project = action.payload.item;
    const files = Object.keys(project.files).map(filename => {
      return _.merge({}, project.files[filename], {filename: filename});
    });
    return { item: _.merge({}, project, { files: files })};
  }
}, {});

export default project;
