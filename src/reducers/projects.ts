import { handleActions, Action } from 'redux-actions';
import * as _ from 'lodash';

import * as ActionTypes from '../actions/ActionTypes';
import { ProjectListPayload } from '../components/ProjectList';

const initialState: ProjectListPayload = {
  items: []
};

const projects = handleActions<ProjectListPayload>({
  [ActionTypes.REQUEST_PROJECTS]: (state, action) => {
    return state;
  },

  [ActionTypes.RECEIVE_PROJECTS]: (state, action) => {

    // プロジェクト整形
    const projects = action.payload.items.map(project => {
      const files = Object.keys(project.files).map(filename => {
        return project.files[filename];
      });
      return _.merge({}, project, { files: files });
    });

    return _.merge({}, action.payload, { items: projects });
  }
}, initialState);

export default projects;
