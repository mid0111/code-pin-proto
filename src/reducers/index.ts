import { combineReducers } from 'redux';
import { routerReducer as routing } from 'react-router-redux';
import { handleActions, Action } from 'redux-actions';

import * as ActionTypes from '../actions/ActionTypes';
import userInfo from './userInfo';
import projects from './projects';
import project from './project';

import { ErrorMessage } from '../components/AlertMessage';

const errorMessage = handleActions<ErrorMessage>({
  [ActionTypes.RECEIVE_ERROR]: (state: ErrorMessage, action: Action<ErrorMessage>): ErrorMessage => {
    return action.payload;
  },

  [ActionTypes.CLOSE_ERROR]:  (state: ErrorMessage, action: Action<ErrorMessage>): ErrorMessage => {
    return {};
  }
}, {});

const rootReducer = combineReducers({
  errorMessage,
  userInfo,
  projects,
  project,
  routing
});

export default rootReducer;
