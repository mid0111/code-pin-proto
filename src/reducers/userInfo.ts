import { handleActions, Action } from 'redux-actions';
import * as _ from 'lodash';

import * as ActionTypes from '../actions/ActionTypes';
import { UserInfo } from '../containers/App';


const initialState: UserInfo = {
  token: undefined,
  user: {},
  fetching: false
};

const userInfo = handleActions<UserInfo>({

  [ActionTypes.LOGIN_REQUEST]: (state: UserInfo, action: Action<UserInfo>): UserInfo => {
    return _.merge({}, state, {
      fetching: true
    });
  },

  [ActionTypes.LOGIN_SUCCESS]: (state: UserInfo, action: Action<UserInfo>): UserInfo => {
    return _.merge({}, state, {
      token: action.payload.token
    });
  },

  [ActionTypes.LOGOUT]: (state: UserInfo, action: Action<UserInfo>): UserInfo => {
    return _.merge({}, initialState);
  },

  [ActionTypes.REQUEST_PROFILE]: (state: UserInfo, action: Action<UserInfo>): UserInfo => {
    return _.merge({}, state, {
      fetching: true
    });
  },

  [ActionTypes.RECEIVE_PROFILE]: (state: UserInfo, action: Action<UserInfo>): UserInfo => {
    return _.merge({}, state, {
      fetching: false,
      user: action.payload
    });
  }

}, initialState);

export default userInfo;
