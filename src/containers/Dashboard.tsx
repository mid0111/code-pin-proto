import * as React from 'react';
import { Component } from 'react';
import { Dispatch } from 'redux';
import { connect } from 'react-redux';

import { Grid, Col, Row, Panel } from 'react-bootstrap';

import { fetchProjects } from '../actions/projects';

import { fetchProjectDetail } from '../actions/project';

import { ProjectDetail, ProjectDetailProps } from '../components/ProjectDetail';
import { ProjectListProps, ProjectList, ProjectListPayload } from '../components/ProjectList';
import { UserInfo } from './App.tsx';

interface DashboardProps {
  projects: ProjectListPayload;
  selectedProject: ProjectDetailProps;
  userInfo: UserInfo;
  dispatch: Dispatch;
}

export class Dashboard extends Component<DashboardProps, {}> {

  constructor(props) {
    super(props);
    this.selectProject = this.selectProject.bind(this);
  }

  componentDidMount() {
    const { dispatch, userInfo } = this.props;
    dispatch(fetchProjects(userInfo.token));
  }

  selectProject(id) {
    const { dispatch, userInfo } = this.props;
    dispatch(fetchProjectDetail(userInfo.token, id));
  }

  render() {
    const { projects, selectedProject } = this.props;

    return (
      <Grid fluid={true}>
        <div className='projects-container sidebar'>
          <ProjectList projects={projects} onSelect={this.selectProject} />
        </div>
        <div className='project-detail-container main'>
          {selectedProject.item ? <ProjectDetail item={selectedProject.item} /> : null}
        </div>
      </Grid>
    );
  }
}

const mapStateToProps = (state: any) => {
  return {
    selectedProject: state.project,
    projects: state.projects,
    userInfo: state.userInfo
  };
};

export default connect(mapStateToProps)(Dashboard);
