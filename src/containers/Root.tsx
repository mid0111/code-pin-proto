import * as React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { Store } from 'redux';
import { Router, Route, IndexRoute, IndexRedirect, Redirect } from 'react-router';
import { ReactRouterReduxHistory } from 'react-router-redux';

import App from '../containers/App';
import Dashboard from '../containers/Dashboard';
import LoginPage from '../components/Login';
import DevTools from '../containers/DevTools';

export interface RootProps {
  store: Store;
  history: ReactRouterReduxHistory;
}

export default class Root extends React.Component<RootProps, {}> {

  renderDevTools() {
    if (process.env.NODE_ENV !== 'production') {
      return (
        <DevTools />
      );
    }
  }

  render() {
    const { store, history } = this.props;
    return (
      <Provider store={store}>
        <div>
          <Router history={history}>
            <Route path='/' component={App}>
              <IndexRedirect to='dashboard' />
              <Route path='dashboard' component={Dashboard} />
              <Route path='login' component={LoginPage} />
              <Redirect from='*' to='dashboard' />
            </Route>
          </Router>
          { this.renderDevTools() }
        </div>
      </Provider>
    );
  }
}
