import * as React from 'react';
import { Component } from 'react';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';

import { requestCloseMessage } from '../actions';
import { checkAuthorized, loginUser } from '../actions/auth';

import { Header, User } from '../components/Header';
import { AlertMessage, ErrorMessage } from '../components/AlertMessage';
import Login from '../components/Login';
import { Loading } from '../components/Loading';

export type UserInfo = {
  token?: string;
  fetching?: boolean;
  user: User;
}

export interface AppProps {
  children: React.ReactElement<any>;
  errorMessage: ErrorMessage;
  userInfo: UserInfo;
  dispatch: Dispatch;
}

const title = 'Code Pin';

export class App extends Component<AppProps, {}> {

  constructor(props: AppProps) {
    super(props);
    this.closeMessage = this.closeMessage.bind(this);
    this.loginRequest = this.loginRequest.bind(this);

    const { dispatch } = props;
    dispatch(checkAuthorized());
  }

  closeMessage() {
    const { dispatch } = this.props;
    dispatch(requestCloseMessage());
  }

  loginRequest(code: string) {
    const { dispatch } = this.props;
    dispatch(loginUser(code));
  }

  renderChildren(children) {
    const { userInfo } = this.props;

    if (userInfo.fetching) {
      return <Loading />;

    } else if (!userInfo.user.login) {
      return <Login onLoginRequest={this.loginRequest} />;

    } else {
      return children;
    }
  }

  render() {
    const { children, errorMessage, userInfo } = this.props;

    return (
      <div>
        <Header title={title} user={ userInfo.user } />
        <AlertMessage errorMessage={errorMessage} onCloseMessage={this.closeMessage} />
        <div className='contents'>
          { this.renderChildren(children) }
        </div>
      </div>
    );
  }

}

const mapStateToProps = (state: any) => {
  const { errorMessage, userInfo } = state;
  return {
    errorMessage,
    userInfo
  };
};

export default connect(mapStateToProps)(App);
