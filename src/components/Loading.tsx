import * as React from 'react';
import { connect } from 'react-redux';

const style = {
};

export class Loading extends React.Component<{}, {}> {

  render() {
    return (
      <div className='container text-center' >
        <i className='fa fa-spinner fa-spin fa-2x' />
      </div>
    );
  }
}

export default connect()(Loading);
