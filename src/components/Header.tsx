import * as React from 'react';
import { Nav, Navbar, NavDropdown, MenuItem } from 'react-bootstrap';

export type User = {
  login?: string;
  avatar_url?: string;
  [others: string]: any;
}

export interface HeaderProps {
  title: string;
  user?: User;
}

export class Header extends React.Component<HeaderProps, {}> {

  constructor(props: HeaderProps) {
    super(props);
    this.renderUserMenu = this.renderUserMenu.bind(this);
  }

  renderUserMenu() {
    const { user } = this.props;
    if (user.login) {
      return (
        <NavDropdown title={user.login} id='user-menu'>
          <MenuItem eventKey={1}>Settings</MenuItem>
          <MenuItem eventKey={2}>Log out</MenuItem>
        </NavDropdown>
      );
    } else {
      return null;
    }
  }

  render() {
    return (
      <Navbar fluid={true} fixedTop={true}>
        <Navbar.Brand className='title'>
          <a href='#'>{this.props.title}</a>
        </Navbar.Brand>
        <Nav pullRight>
          {  this.renderUserMenu() }
        </Nav>
      </Navbar>
    );
  }
}
