import * as React from 'react';

const Highlight = require('react-highlight');

type File = {
  size: number,
  raw_url: string,
  language: string,
  type: string,
  filename: string,
  content?: string,
  trancated?: boolean
};

export type ProjectInfo = {
  id: string,
  truncated?: boolean,
  owner: {
    avatar_url: string;
    id: number;
    login: string;
    [others: string]: any;
  },
  description?: string;
  updated_at: string;
  created_at: string;
  files: File[];
  html_url: string;
  git_push_url: string;
  git_pull_url: string;
  commits_url: string;
  forks_url: string;
  url: string;
  [others: string]: any;
};

export interface ProjectDetailProps {
  item?: ProjectInfo;
};

export class ProjectDetail extends React.Component<ProjectDetailProps, {}> {

  getMode(language: string) {
    return language.toLowerCase();
  }

  render() {
    const { item } = this.props;

    return (
      <div>
      <h5 className='project-description'>{item.description}</h5>
      {item.files.map((file, index) =>
        <div key={'files-' + index}>
          <h6 className=''>{file.filename}</h6>
          <Highlight className={this.getMode(file.language)}>
            {file.content}
          </Highlight>
        </div>
      )}
      </div>
    );
  }
}
