import * as React from 'react';

import { Project } from './Project';
import { ProjectInfo } from './ProjectDetail';

export interface ProjectListPayload {
  items: ProjectInfo[];
  next?: string;
}

export interface ProjectListProps {
  projects: ProjectListPayload;
  onSelect: Function;
}

interface ProjectListState {
  activeId: string;
}

export class ProjectList extends React.Component<ProjectListProps, ProjectListState> {

  constructor(props) {
    super(props);
    this.state = {
      activeId:  null
    };
  }

  onSelect(id) {
    this.setState({activeId: id});
    this.props.onSelect(id);
  }

  render() {
    const { projects } = this.props;
    return (
      <div>
        {projects.items.map((project) => {
          const isActive = project.id === this.state.activeId;
          return <Project onClick={this.onSelect.bind(this, project.id)} project={project} isActive={isActive} key={project.id} />;
        }
      )}
      </div>
    );
  }
}
