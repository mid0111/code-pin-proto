import * as React from 'react';

import { Button, Modal } from 'react-bootstrap';

export type ErrorMessage = {
  message?: string;
  detail?: string;
  error?: Error;
};

export interface AlertMessageProps {
  errorMessage: ErrorMessage;
  onCloseMessage: Function;
}

export class AlertMessage extends React.Component<AlertMessageProps, {}> {

  render() {
    const { errorMessage, onCloseMessage } = this.props;
    let show = errorMessage.message ? true : false;

    return (
      <Modal show={show} onHide={onCloseMessage}>
        <Modal.Header closeButton>
          <Modal.Title className='alert-title'>{errorMessage.message}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <p className='alert-detail'>{errorMessage.detail ? errorMessage.detail : ''}</p>
        </Modal.Body>
      </Modal>
    );
  }
}
