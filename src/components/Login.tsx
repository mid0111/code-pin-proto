import { remote } from 'electron';
const BrowserWindow = remote.BrowserWindow;

import * as React from 'react';
import { connect } from 'react-redux';

import { github } from '../utils/consts';
import { githubSecret } from '../utils/secretConsts';

interface LoginProps {
  onLoginRequest: Function;
};

const style = {
  container: {
    marginTop: '100px'
  },
  button: {
    minWidth: '300px'
  },
  title: {
    marginLeft: '.6em'
  }
};

export class LoginPage extends React.Component<LoginProps, {}> {

  constructor(props: LoginProps) {
    super(props);
    this.authGithub = this.authGithub.bind(this);
  }

  authGithub () {
    const { onLoginRequest } = this.props;

    // Build the OAuth consent page URL
    const authWindow = new BrowserWindow({
      width: 800,
      height: 600,
      show: true,
      webPreferences: {
        nodeIntegration: false
      }
    });
    let authUrl = github.authorizeUrl + '?client_id=' + githubSecret.clientId + '&scope=' + github.scope;
    authWindow.loadURL(authUrl);

    // If 'Done' button is pressed, hide 'Loading'
    authWindow.on('close', function () {
      authWindow.destroy();
    });

    authWindow.webContents.on('will-navigate', function (event, url) {
      handleCallback(url);
    });

    authWindow.webContents.on('did-get-redirect-request', function (event, oldUrl, newUrl) {
      handleCallback(newUrl);
    });

    function handleCallback (url) {
      let rawCode = /code=([^&]*)/.exec(url) || null;
      let code = (rawCode && rawCode.length > 1) ? rawCode[1] : null;
      let error = /\?error=(.+)$/.exec(url);

      if (code || error) {
        // Close the browser if code found or error
        authWindow.destroy();
      }

      // If there is a code, proceed to get token from github
      if (code) {
        onLoginRequest(code);
      } else if (error) {
        alert('Oops! Something went wrong and we couldn\'t ' +
          'log you in using Github. Please try again.');
      }
    }
  }

  render() {
    return (
      <div className='container login center-block' style={style.container}>
        <div className='login-button'>
          <button className='btn btn-secondary btn-lg btn-block' onClick={this.authGithub} style={style.button}>
              <i className='fa fa-github' /><span style={style.title}>Log in to GitHub</span>
          </button>
        </div>
      </div>
    );
  }
}

export default connect()(LoginPage);
