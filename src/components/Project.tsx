import * as React from 'react';

import { Grid, Col, Row, Panel } from 'react-bootstrap';
import { ProjectInfo } from './ProjectDetail';

type ProjectProps = {
  project: ProjectInfo,
  onClick?: Function,
  isActive: boolean
};

export class Project extends React.Component<ProjectProps, {}> {

  render() {
    const { project, onClick, isActive } = this.props;
    const classNameActive = isActive ? 'is-active' : '';

    const firstFile = project.files[0];
    return (
      <div onClick={onClick} className={'project-item iterable-item ' + classNameActive}>
        <h6 className='project-description'>{project.description}</h6>
        <p className='project-name'>{firstFile.filename}</p>
      </div>
    );
  }
}
