
function getItem(key) {
  return window.localStorage.getItem(key);
}

function setItem(key, value) {
  return window.localStorage.setItem(key, value);
}

export {
  getItem,
  setItem
}
