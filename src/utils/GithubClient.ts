import * as request from 'request';
import * as Promise from 'bluebird';
import { github } from '../utils/consts';
import { githubSecret } from '../utils/secretConsts';
const parseLinkHeader = require('parse-link-header');

interface IGithubClient {
  getUser(): Promise<any>;
}

export default class GithubClient implements IGithubClient {
  token: string;

  constructor(token?: string) {
    this.token = token;
  }

  request(method: string, path: string): Promise<any> {
    return new Promise((resolve, reject) => {
      request(github.baseUrl + path, {
        method: method,
        headers: {
          'Authorization': 'token ' + this.token,
          'Accept': 'application/json',
          'User-Agent': 'CodePin'
        }
      }, (err, res, body) => {
        if (err) {
          return reject(err);
        }
        if (res.statusCode >= 400) {
          return reject(new Error(body));
        }
        return resolve({
          body: JSON.parse(body),
          headers: res.headers
        });
      });
    });
  }

  get(path: string): Promise<any> {
    return this.request('GET', path);
  }

  getToken(code: string): Promise<any> {
    return new Promise((resolve, reject) => {
      request.post(github.oauthUrl, {
        headers: {
          'Accept': 'application/json'
        },
        json: {
          'client_id': githubSecret.clientId,
          'client_secret': githubSecret.secret,
          'code': code
        }
      }, (err, res, body) => {
        if (err) {
          reject(err);
        }
        if (res.statusCode >= 400) {
          reject(new Error(body));
        }
        resolve(body.access_token);
      });
    });
  }

  getUser(): Promise<any> {
    return this.get('/user')
      .then(res => res.body);
  }

  getGists(path?: string): Promise<any> {
    return this.get(path || '/gists')
      .then(res => {
        const gists = res.body;

        const link: Link = parseLinkHeader(res.headers.link);
        let next;
        if (link && link.next) {
          next = link.next.url.split(github.baseUrl)[1];
        }

        return {
          gists: gists,
          next: next
        };
      });
  }

  getGistDetail(id): Promise<any> {
    return this.get('/gists/' + id)
      .then(res => res.body);
  }
}

interface LinkObject {
  page: string;
  per_page: string;
  rel: string;
  url: string;
}

interface Link {
  next?: LinkObject;
  prev?: LinkObject;
  last?: LinkObject;
}
