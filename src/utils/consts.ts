export const github = {
  baseUrl: 'https://api.github.com',
  authorizeUrl: 'https://github.com/login/oauth/authorize',
  oauthUrl: 'https://github.com/login/oauth/access_token',
  scope: 'gist',
  store: 'gh_access_token'
};
