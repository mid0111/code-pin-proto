import * as request from 'request';
import * as Promise from 'bluebird';
import { Dispatch } from 'redux';
import { createAction } from 'redux-actions';

import * as ActionTypes from './ActionTypes';
import { receiveError } from './index';

import GithubClient from '../utils/GithubClient';

const errorMessage = 'Failed to load project detail.';

const requestProjectDetail = createAction<void>(
  ActionTypes.REQUEST_PROJECT_DETAIL
);

const receiveProjectDetail = createAction<any>(
  ActionTypes.RECEIVE_PROJECT_DETAIL,
  (project: any) => {
    return {
      item: project
    };
  }
);

function fetchProjectDetail(token, id) {
  return (dispatch: Dispatch) => {
    dispatch(requestProjectDetail());

    const githubClient = new GithubClient(token);
    return githubClient.getGistDetail(id)
      .then(body => dispatch(receiveProjectDetail(body)))
      .catch(err => dispatch(receiveError(errorMessage, err)));
  };
}

export {
  fetchProjectDetail
}
