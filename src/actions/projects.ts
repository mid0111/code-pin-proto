import * as request from 'request';
import * as Promise from 'bluebird';
import { Dispatch } from 'redux';
import { createAction } from 'redux-actions';

import * as ActionTypes from './ActionTypes';
import { receiveError } from './index';

import GithubClient from '../utils/GithubClient';

const errorMessage = 'Failed to load projects.';

const requestProjects = createAction<any>(
  ActionTypes.REQUEST_PROJECTS
);

const receiveProjects = createAction<any>(
  ActionTypes.RECEIVE_PROJECTS,
  (res: any) => {
    return {
      items: res.gists,
      next: res.next
    };
  }
);

function fetchProjectsAll(githubClient, gists, dispatch, next?) {
  dispatch(requestProjects());
  return githubClient.getGists(next)
    .then(body => {
      const concat = gists.concat(body.gists);

      dispatch(receiveProjects({
        gists: concat,
        next: body.next
      }));

      if (body.next) {
        return fetchProjectsAll(githubClient, concat, dispatch, body.next);
      }
    })
    .catch(err => dispatch(receiveError(errorMessage, err)));
}

function fetchProjects(token) {
  return (dispatch: Dispatch) => {
    const githubClient = new GithubClient(token);
    const gists = [];
    return fetchProjectsAll(githubClient, gists, dispatch);
  };
}

export {
  fetchProjects
}
