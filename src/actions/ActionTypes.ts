export const LOADING = 'LOADING';
export const LOADING_END = 'LOADING_END';

export const REQUEST_PROJECTS = 'REQUEST_PROJECTS';
export const RECEIVE_PROJECTS = 'RECEIVE_PROJECTS';

export const REQUEST_PROJECT_DETAIL = 'REQUEST_PROJECT_DETAIL';
export const RECEIVE_PROJECT_DETAIL = 'RECEIVE_PROJECT_DETAIL';

export const RECEIVE_ERROR = 'RECEIVE_ERROR';
export const CLOSE_ERROR = 'CLOSE_ERROR';

export const LOGIN_REQUEST = 'LOGIN_REQUEST';
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGIN_FAILURE = 'LOGIN_FAILURE';
export const LOGOUT = 'LOGOUT';

export const REQUEST_PROFILE = 'REQUEST_PROFILE';
export const RECEIVE_PROFILE = 'RECEIVE_PROFILE';
