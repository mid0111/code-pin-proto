import * as ActionTypes from './ActionTypes';
import { createAction } from 'redux-actions';

import { ErrorMessage } from '../components/AlertMessage';

const loading = createAction<void>(
  ActionTypes.LOADING
);

const requestCloseMessage = createAction<void>(
  ActionTypes.CLOSE_ERROR
);

const receiveError = createAction<ErrorMessage>(
  ActionTypes.RECEIVE_ERROR,
  (message, err: Error) => {
    return {
      message: message,
      detail: err.message,
      err: err
    };
  }
);

export {
  receiveError,
  requestCloseMessage
}
