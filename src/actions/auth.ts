import { remote } from 'electron';

import * as request from 'request';
import * as Promise from 'bluebird';
import { Dispatch } from 'redux';
import { createAction } from 'redux-actions';

import * as ActionTypes from './ActionTypes';
import { github } from '../utils/consts';

import { ErrorMessage } from '../components/AlertMessage';
import { UserInfo } from '../containers/App';

import * as Storage from '../utils/storage';
import GithubClient from '../utils/GithubClient';

const receiveToken = createAction<any>(
  ActionTypes.LOGIN_SUCCESS,
  (token: string) => {
    return { token };
  }
);

function checkAuthorized() {
  return (dispatch: Dispatch) => {

    // session 情報を元にトークンを取得する
    // ここでエラーがあっても再度ログイン画面を表示するため無視する
    const storedToken = Storage.getItem(github.store);
    dispatch(receiveToken(storedToken));

    return dispatch(fetchProfile(storedToken));
  };
}

const requestProfile = createAction<void>(
  ActionTypes.REQUEST_PROFILE
);

const receiveProfile = createAction<any>(
  ActionTypes.RECEIVE_PROFILE,
  (profile) => profile
);

const fetchProfileFail = (err: Error) => {
  return (dispatch: Dispatch) => {
    dispatch(logout());
  };
};

function fetchProfile(token) {
  return (dispatch: Dispatch) => {
    dispatch(requestProfile());

    const githubClient = new GithubClient(token);
    return githubClient.getUser()
      .then(body => dispatch(receiveProfile(body)))
      .catch(err => dispatch(fetchProfileFail(err)));
  };
}

const requestLogin = createAction<void>(
  ActionTypes.LOGIN_REQUEST
);

const loginSuccess = createAction<any>(
  ActionTypes.LOGIN_SUCCESS,
  (res: UserInfo) => res
);

const loginFail = createAction<ErrorMessage>(
  ActionTypes.LOGIN_FAILURE,
  (err: Error) => {
    return {
      message: 'Failed to login.',
      detail: err.message,
      err: err
    };
  }
);

export function loginUser(code) {
  return (dispatch: Dispatch) => {
    dispatch(requestLogin());

    const githubClient = new GithubClient();
    return githubClient.getToken(code)
      .then((token) => {
        dispatch(loginSuccess({
          token
        }));

        Storage.setItem(github.store, token);

        return dispatch(fetchProfile(token));
      })
      .catch(err => dispatch(loginFail(err)));
  };
}

const logout = createAction<void>(
  ActionTypes.LOGOUT
);

export {
  checkAuthorized,
  logout
}
