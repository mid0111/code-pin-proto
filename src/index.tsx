import * as React from 'react';
import { render } from 'react-dom';
import { browserHistory } from 'react-router';
import { syncHistoryWithStore } from 'react-router-redux';

import configureStore from './configStore';
import Root from './containers/Root';

import 'font-awesome/css/font-awesome.css';
import './styles/bootstrap/css/bootstrap.min.css';
import './styles/main.styl';
import 'highlight.js/styles/github.css';

const store = configureStore();
const history = syncHistoryWithStore(browserHistory, store);

render(
    <Root store={store} history={history} />,
  document.getElementById('App')
);
