import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import rootReducer from './reducers';

import DevTools from './containers/DevTools';

// Middleware
const middlewares = applyMiddleware(
  thunk
);

let enhancer;
if (process.env.NODE_ENV === 'production') {
  enhancer = compose(middlewares);
} else {
  enhancer = compose(middlewares,
    // Required! Enable Redux DevTools with the monitors you chose
    DevTools.instrument()
  );
}

function configureStore(initialState = {}) {
  return createStore(rootReducer, initialState, enhancer);
}

export default configureStore;
