Code Pin
====

[![wercker status](https://app.wercker.com/status/76b21bdf1b55c745f78bcac56b0f18c0/s/master "wercker status")](https://app.wercker.com/project/bykey/76b21bdf1b55c745f78bcac56b0f18c0)

react + redux + typescript + webpack らへんで遊ぶプロジェクト  

できたら gist と連携してコード snippet をビジュアライズしつつ管理するアプリまで育てる

## Requirement

* Machine: Unix
* node: >= 4.x

## Development

1. Start watch server.  
     ```bash
     npm install
     npm run dev
     ```
2. Start app.  
     ```bash
     npm start
     ```

## Build

```bash
npm run package
```
