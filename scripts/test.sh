#!/bin/sh

mv ./src/utils/secretConsts.ts ./src/utils/secretConsts.ts.org
cp ./src/utils/secretConsts.sample.ts ./src/utils/secretConsts.ts

tsc || exit $
mocha ./.tmp/test/**/*.spec.js || exit $

if [ -f ./src/utils/secretConsts.ts.org ]; then
    mv ./src/utils/secretConsts.ts.org ./src/utils/secretConsts.ts
fi
