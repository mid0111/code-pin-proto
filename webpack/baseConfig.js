var path = require('path');
var webpack = require('webpack');
var ExtractTextPlugin = require('extract-text-webpack-plugin');

var nodeModulesPath = path.join(__dirname, '../node_modules');

var config = {
  entry: {
    app: ['./src/index.tsx']
  },

  target: 'electron',

  resolve: {
    extensions: ['', '.tsx', '.ts', '.js', '.styl', '.css'],
    root: [
      path.resolve('../src')
    ]
  },

  module: {
    // request.js の先で起こるエラー[define cannot be used indirect] の対応
    // see - https://github.com/request/request/issues/1920
    noParse: /node_modules\/json-schema\/lib\/validate\.js/,

    preLoaders: [{
      test: /\.tsx?$/,
      include: path.resolve(__dirname, '../src'),
      loader: "tslint"
    }],

    loaders: [{
      test: /\.tsx?$/,
      include: path.resolve(__dirname, '../src'),
      loader: "ts-loader"
    }, {
      test  : /\.styl$/,
      include: path.resolve(__dirname, '../src'),
      loaders: ['style', 'css', 'stylus']
    }, {
      test: /\.css$/,
      loader: ExtractTextPlugin.extract('style', 'css')
    }, {
      test: /\.(eot|svg|ttf|woff(2)?)(\?v=\d+\.\d+\.\d+)?/,
      loader: 'url'
    }, {
      test: /\.json$/,
      loader: "json-loader"
    }]
  },

  tslint: {
    // Rules are in tslint.json
    emitErrors: true
  },

  plugins: [
    new ExtractTextPlugin('bundle.css')
  ]
};

module.exports = {
  config
};
