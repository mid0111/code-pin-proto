var path = require('path');
var webpack = require('webpack');
var config = require('./baseConfig').config;
var buildDir = path.resolve(__dirname, '..');

// Enable sourcemaps for debugging webpack's output.
config.devtool = 'source-map';

// Add webpack-dev-server client entry point.
config.entry.app.unshift('webpack-dev-server/client?http://localhost:8080/', 'webpack/hot/dev-server');

config.output = {
  path: buildDir,
  publicPath: 'http://localhost:8080/dist/',
  filename: 'bundle.js'
};

config.devServer = {
  contentBase: buildDir,

  // Enable history API fallback so HTML5 History API based
  // routing works. This is a good default that will come
  // in handy in more complicated setups.
  historyApiFallback: true,
  hot: true,
  inline: true,
  progress: true
};

config.plugins.unshift(new webpack.HotModuleReplacementPlugin());

module.exports = config;
