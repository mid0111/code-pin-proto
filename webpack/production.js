var path = require('path');
var webpack = require("webpack");
var config = require('./baseConfig').config;

var distDir = path.join(__dirname, '../build/dist');

config.output = {
  path: distDir,
  publicPath: '/dist/',
  filename: 'bundle.js'
};

config.plugins.unshift(
  new webpack.DefinePlugin({
    'process.env':{
      'NODE_ENV': JSON.stringify('production')
    }
  }),
  new webpack.optimize.UglifyJsPlugin({
    compress:{
      warnings: false
    }
  })
);

module.exports = config;
