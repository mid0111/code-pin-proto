var path = require('path');
var webpack = require("webpack");
var CopyWebpackPlugin = require('copy-webpack-plugin');

var rootDir = path.join(__dirname, '..');
var buildDir = path.join(rootDir, 'build');

var config = {
  entry: './main.js',

  target: 'electron-main',

  resolve: {
    extensions: ['', 'js'],
    root: [
      path.resolve('..')
    ]
  },

  output: {
    path: buildDir,
    filename: 'main.js'
  },

  node: {
    __dirname: false,
    __filename: false
  },

  plugins: [
    new webpack.DefinePlugin({
      'process.env':{
        'NODE_ENV': JSON.stringify('production')
      }
    }),
    new webpack.optimize.UglifyJsPlugin({
      compressor: {
        warnings: false
      }
    }),
    new CopyWebpackPlugin([{
      from: path.join(rootDir, 'index.html'),
      to: buildDir
    },{
      from: path.join(rootDir, 'package.json'),
      to: buildDir
    }])
  ]
};

module.exports = config;
